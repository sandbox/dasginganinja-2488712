<?php

/**
 * @file
 * Token callbacks for the translated_source_tokens module.
 */

/**
 * Implements hook_token_info().
 *
 * @ingroup translated_source_tokens
 */
function translated_source_tokens_token_info() {
  $info['tokens']['node']['source-alias'] = array(
    'name' => t('Translated Source Path Alias'),
    'description' => t('Provides the path alias for the translation source if it exists'),
  );

  return $info;
}

/**
 * Implements hook_tokens().
 *
 * @ingroup translated_source_tokens
 */
function translated_source_tokens_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  $sanitize = !empty($options['sanitize']);

  // Node tokens.
  if ($type == 'node' && !empty($data['node'])) {
    $node = $data['node'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
      case 'parent-alias':
        //$aliases = translated_source_tokens_get_translated_source_aliases($node->tnid);
        //$alias = '';
        //if (count($aliases)) {
        //  $alias = reset($aliases);
        //}

        // Lookup alias of translated node using Drupal System Path
        $node_path = sprintf('node/%d',$node->tnid);
        $translated_node_language = NULL;
        $alias = drupal_lookup_path('alias', $node_path, $translated_node_language);

        $replacements[$original] = $alias;
        break;
      default:
        break;
      }
    }

    return $replacements;
  }
}

function translated_source_tokens_get_translated_source_aliases($tnid) {
  if (!user_access('access content') && !user_access('bypass node access')) {
    return array();
  }

  $node_query = db_select('node', 'n');
  $node_query->addExpression("CONCAT('node/', n.nid)", 'nodepath');
  $node_query->leftJoin('url_alias','u', 'u.source = nodepath');
  $node_query->fields('u', array('alias'));
  $node_query->condition('n.nid',$tnid,'=');
  $node_query->addTag('node_access');
  $nodes = $node_query->execute()->fetchAllKeyed();
  $nodes = array_map('check_plain', $nodes);
  return $nodes;
}
